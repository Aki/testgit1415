class Box:
    def __init__(self):
        self._contents = []
        self.status = "open"
        self._capacity = None
    
    def add(self,truc):
        if self.status == "open":
            self._contents.append(truc)
        else:
            pass
        
    def __contains__(self,truc):
        return truc in self._contents
    
    def remove(self,truc):
        self._contents.remove(truc)
        
    def close(self):
        self.status="close"
    
    def open(self):
        self.status="open"
        
    def is_open(self):
        return self.status == "open"
    
    
    def action_look(self):
        if self.status == "close":
            return "La boite est fermée"
        elif len(self._contents) == 0:
            return "la boite est vide"
        else:
            res="La boite contient: "
            for i in range(len(self._contents)):
                res +=str(self._contents[i])+", "
            return res

    def set_capacity(self,place):
        self._capacity = place
        
    def capacity(self):
        return self._capacity

    def has_room_for(self,t):
        if self._capacity == None:
            return True
        elif t<= self._capacity:
            self._capacity=self._capacity-t
            return True
        else:
            return False
        
    def action_add(self,chose):
        poid=chose.volume()
        if self.has_room_for(poid) and self.is_open:
            self._contents.append(chose)
            return True
        else:
            return False

    def find(self,nom):
		if nom in self._contents:
			return nom
		else:
			return None
       
   
            
    
        
    
        
# ajout d'un objet dans une boite
class Thing:
    def __init__(self,poid=1,name="bidule"):
        self._poid=poid
        self.name=name
            
    def volume(self):
        return self._poid
    
    def set_name(self,name):
        self.name=name    

    def has_name(self,name):
        if self.name==name:
            return True
        else:
            return False
