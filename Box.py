class Box:
	def __init__(self,capacity=None,is_open=None):
		self.capacity=capacity
		self.is_open=is_open
	
	@staticmethod
	def construction(dico):
		cap=dico.get("capacity", None)
		op=dico.get("is_open", None)
		return Box(capacity=cap, is_open=op)

#Coucou ceci est une modification, et encore une autre modif'.
